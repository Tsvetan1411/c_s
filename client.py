import socket
import orjson


class Client:
    def __init__(self, cl_host, cl_port, key, logic):
        self.host = cl_host
        self.port = cl_port
        self.verification_key = key
        self.logic = logic
        self.serv_conn = None

    def start_client(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.host, self.port))
            s.sendall(self.verification_key)
            self.serv_conn = orjson.loads(s.recv(1024))
            if self.serv_conn:
                print(orjson.loads(s.recv(1024)))
            while self.serv_conn:
                user_input = input("Type command: ")

                if user_input.lower() == "log":
                    if self.logic.logged_user:
                        print("You are allready logged in!")
                        continue

                if user_input.lower() == "stop":
                    send_data = {"command": "stop"}
                    s.sendall(orjson.dumps(send_data))
                    break

                send_data = self.logic.send_data_manager(user_input)
                s.sendall(orjson.dumps(send_data))
                recv_data = orjson.loads(s.recv(1024))
                self.logic.recv_data_manager(recv_data)


class Logic:
    def __init__(self):
        self.logged_user = None
        self.method_dict = {
            "create": self.new_user,
            "log": self.log_user,
            "log out": self.logout_user,
            "deactivate user": self.deactivate_user,
            "send": self.send_message
        }

    def send_data_manager(self, user_input):
        if user_input.lower() in self.method_dict:
            send_data = self.method_dict[user_input]()
        else:
            send_data = {"command": user_input}
        return send_data


    def recv_data_manager(self, recv_data):
        if "message" in recv_data:
            print(recv_data["message"])

        if "command" in recv_data:
            if recv_data["command"] == "log user":
                data = recv_data["data"]
                name = data
                self.logged_user = User(name, "*")
            elif recv_data["command"] == "users list":
                print("user/is admin/is active")
                for user, status in recv_data["data"].items():
                    print(user, status)
            elif recv_data["command"] == "mailbox content":
                for message in recv_data["data"]:
                    print(f"Send: {message[0]}")
                    if message[1] == True:
                        print("New")
                    else:
                        print("Readed")
                    print(f"From: {message[2]}")
                    print(f"Message: {message[3]}")

    def new_user(self):
        user_name = input("Enter user name: ")
        user_password = input("Enter user password: ")
        new_user = User(user_name, user_password)
        return {"command": "new user",
                "data": new_user.get_info()
        }

    def log_user(self):
        user_name = input("Enter user name: ")
        user_password = input("Enter user password: ")
        return {"command": "log user",
                "data": (user_name, user_password)
        }

    def logout_user(self):
        self.logged_user = None
        return {"command": "log out"}

    def deactivate_user(self):
        user_name = input("Enter user name: ")
        return {
            "command": "deactivate user",
            "data": user_name
        }

    def send_message(self):
        receiver = input("Enter receiver name: ")
        message = input("Enter message: ")
        return {
            "command": "send message",
            "data": (receiver, self.logged_user.name, message)
        }


class User:
    def __init__(self, name, password, admin=False, active=True):
        self.name = name
        self.password = password
        self.admin = admin
        self.active = active

    def get_info(self):
        return self.name, self.password, self.admin, self.active
                

host = "127.0.0.1"
port = 65432

if __name__ == "__main__":
    cl_logic = Logic()
    new_client = Client(host, port, b"1234", cl_logic)
    new_client.start_client()
