import unittest
from unittest.mock import patch, MagicMock
from logic import *


class TestServerMainLogic(unittest.TestCase):
    def setUp(self):
        self.server_main_logic = ServerMainLogic()
        self.mock_db_conn = MagicMock()
        self.server_main_logic.DBconn = self.mock_db_conn

    @patch.object(ServerMainLogic, 'hash_and_salt_password')
    def test_create_user_in_server_main_logic_with_unique_name(self, mock_salt_hash):
        self.mock_db_conn.commands.create_user.return_value = True
        test_data = ["new user", "****", False, True]
        result = self.server_main_logic.create_user(test_data)
        self.assertEqual(result, {"message": "User was created successfully"})

    @patch.object(ServerMainLogic, 'hash_and_salt_password')
    def test_create_user_in_server_main_logic_with_not_unique_name(self, mock_salt_hash):
        self.mock_db_conn.commands.create_user.return_value = False
        test_data = ["user", "****", False, True]
        result = self.server_main_logic.create_user(test_data)
        self.assertEqual(result, {"message": "Your name is not unique"})

    def test_log_user_in_server_main_logic_with_valid_name_and_password(self):
        self.mock_db_conn.commands.get_user_info.return_value =(
                "user",
                b'$2b$12$WOuq5.eMz0fbGE9nizajV.FS17VUHdsDDmRZ7v/Unv2/u0KVWVcXq',
                True,
                True,
                3
        )
        test_data = ["user", "user password"]
        result = self.server_main_logic.log_user(test_data)
        self.assertIsInstance(self.server_main_logic.logged_user, LoggedUser)
        self.assertEqual(
            result,
            {
                "command": "log user",
                "message": "Logged in successfully",
                "data": "user"
            }
        )

    @patch('bcrypt.checkpw', return_value=True)
    def test_log_user_in_server_main_logic_with_invalid_name(self, mock_check_pw):
        self.mock_db_conn.commands.get_user_info.return_value = None
        test_data = ["user", "user password"]
        result = self.server_main_logic.log_user(test_data)
        self.assertEqual(result, {"message": "Wrong user name."})

    def test_log_user_in_server_main_logic_with_invalid_password(self):
        self.mock_db_conn.commands.get_user_info.return_value =(
                "user",
                b'$2b$12$WOuq5.eMz0fbGE9nizajV.FS17VUHdsDDmRZ7v/Unv2/u0KVWVcXq',
                True,
                True,
                3
        )
        test_data = ["user", "user invalid password"]
        result = self.server_main_logic.log_user(test_data)
        self.assertEqual(result, {"message": "Invalid password!"})

    def test_log_user_in_server_main_account_blocked_by_admin(self):
        self.mock_db_conn.commands.get_user_info.return_value =(
                "user",
                b'$2b$12$WOuq5.eMz0fbGE9nizajV.FS17VUHdsDDmRZ7v/Unv2/u0KVWVcXq',
                True,
                False,
                3
        )
        test_data = ["user", "user password"]
        result = self.server_main_logic.log_user(test_data)
        self.assertEqual(result, {"message": "Your account was blocked by admin"})