import unittest
from unittest.mock import patch
from client import *
from contextlib import redirect_stdout
from io import StringIO


class TestClient(unittest.TestCase):

    def setUp(self):
        self.client_logic = Logic()
        self.client_logic.logged_user = User("name", "****")

    def capture_print(self, method, data=None):
        output_capture = StringIO()
        with redirect_stdout(output_capture):
            method(data)
        actual_printout = output_capture.getvalue().rstrip()
        output_capture.close()
        return actual_printout

    def test_recv_data_manager_prints_expected_message(self):
        testing_data = {"message": "hello world"}
        actual_printout = self.capture_print(self.client_logic.recv_data_manager, testing_data)
        self.assertEqual(actual_printout, "hello world")

    def test_recv_data_manager_handle_command_log_user(self):
        testing_data = {
            "command": "log user",
            "data": "user"
        }
        self.client_logic.recv_data_manager(testing_data)
        self.assertEqual(
            self.client_logic.logged_user.get_info(),
            ("user", "*", False, True)
        )

    def test_recv_data_manager_handle_command_users_list(self):
        testing_data = {"command": "users list", "data": {"user": [True, True]}}
        actual_printout = self.capture_print(self.client_logic.recv_data_manager, testing_data)
        self.assertEqual(
            actual_printout,
            "user/is admin/is active\nuser [True, True]"
        )

    def test_recv_data_manager_handle_command_mailbox_content(self):
        testing_data = {
            "command": "mailbox content",
            "data": [["01.02.2024", True, "Sender", "Spam"]]
        }
        actual_printout = self.capture_print(self.client_logic.recv_data_manager, testing_data)
        self.assertEqual(
            actual_printout,
            "Send: 01.02.2024\nNew\nFrom: Sender\nMessage: Spam"
        )

    @patch("builtins.input", side_effect=["new user", "new password"])
    def test_create_new_user_in_client_logic(self, mock_input):
        result = self.client_logic.new_user()
        self.assertEqual(
            result,
            {
                "command": "new user",
                "data": ("new user", "new password", False, True)
            }
        )

    @patch("builtins.input", side_effect=["user", "password"])
    def test_log_user_in_client_logic(self, mock_input):
        result = self.client_logic.log_user()
        self.assertEqual(
            result,
            {
                "command": "log user",
                "data": ("user", "password")
            }
        )

    def test_logout_user_in_client_logic(self):
        result = self.client_logic.logout_user()
        self.assertEqual(self.client_logic.logged_user, None)
        self.assertEqual(result, {"command": "log out"})

    @patch("builtins.input", side_effect=["user name"])
    def test_deactivate_user_in_client_logic(self, mock_imput):
        result = self.client_logic.deactivate_user()
        self.assertEqual(
            result,
            {
                "command": "deactivate user",
                "data": "user name"
            }
        )

    @patch("builtins.input", side_effect=["recivers name", "message"])
    def test_send_message_in_client_logic(self, mock_imput):
        self.client_logic.logged_user.name = "from"
        result = self.client_logic.send_message()
        self.assertEqual(
            result,
            {
                "command": "send message",
                "data": ("recivers name", "from", "message")
            }
        )