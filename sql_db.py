import psycopg2
from config import load_config
import sqlite3


class DatabaseConnection:
    def __init__(self, db_path):
        self.db_path = db_path
        self.conn = None
        self.cur = None
        self.commands = None
        self.user_commands = None

    def open_connection(self):
        try:
            self.conn = sqlite3.connect(self.db_path)
            self.cur = self.conn.cursor()
            self.commands = DatabaseMainCommands(self.conn)
            self.user_commands = DatabaseUserCommands(self.conn)
            self.commands.create_tables_at_first_run()
            print("Connection to db opened successfully.")
        except Exception as e:
            print(f"An error occurred while connecting to the db: {e}")

    def close_connection(self):
        if self.cur is not None:
            self.cur.close()
            print("Cursor closed.")
        if self.conn is not None:
            self.conn.close()
            print("Connection to db closed.")


class DatabaseMainCommands:
    def __init__(self, conn):
        self.conn = conn
        self.cursor = self.conn.cursor()

    def create_tables_at_first_run(self):
        query = """
        CREATE TABLE IF NOT EXISTS users (
            username VARCHAR(255) NOT NULL PRIMARY KEY,
            password BYTEA NOT NULL,
            is_admin BOOLEAN NOT NULL DEFAULT FALSE,
            is_active BOOLEAN NOT NULL DEFAULT TRUE,
            unread_mails INTEGER NOT NULL DEFAULT 0 CHECK (unread_mails <= 5)
        );
        """
        self.cursor.execute(query)
        self.conn.commit()

    def get_notification(self, notification_key):
        query = "SELECT notification FROM notifications WHERE key = ?;"
        self.cursor.execute(query, (notification_key,))
        notification = self.cursor.fetchone()
        result = notification[0].replace("\\n", "\n")
        return result

    def create_user(self, username, password):
        query = "INSERT INTO users (username, password) VALUES (?, ?);"
        try:
            self.cursor.execute(query, (username, password))
            self.conn.commit()
            self.create_mailbox(username)
            return True
        except psycopg2.IntegrityError:
            self.conn.rollback()
            return False

    def create_mailbox(self, username):
        query = f"""
        CREATE TABLE IF NOT EXISTS {username}_mailbox (
            recive TIMESTAMP NOT NULL PRIMARY KEY,
            is_new BOOLEAN NOT NULL DEFAULT TRUE,
            sender VARCHAR(255) NOT NULL,
            content VARCHAR(255) NOT NULL
        );
        """
        self.cursor.execute(query)
        self.conn.commit()

    def get_user_info(self, user_name):
        query = """SELECT username, password, is_admin, is_active, unread_mails FROM users WHERE username = ?;"""
        self.cursor.execute(query, (user_name,))
        users_record = self.cursor.fetchone()
        return users_record


class DatabaseUserCommands:
    def __init__(self, conn):
        self.conn = conn
        self.cursor = self.conn.cursor()

    def get_all_users_info(self):
        query = "SELECT username, is_admin, is_active FROM users;"
        self.cursor.execute(query)
        all_users_info = self.cursor.fetchall()
        return all_users_info

    def deactivate_user(self, user_name):
        query = "UPDATE users SET is_active = FALSE WHERE username = ?;"
        try:
            self.cursor.execute(query, (user_name,))
            self.conn.commit()
        except Exception as e:
            print(f"Error while deactivating user: {e}")
            self.conn.rollback()

    def recv_message(self, receiver, sender, message):
        receiver_table_name = f"{receiver}_mailbox"
        query_recv = f"INSERT INTO {receiver_table_name} (recive, sender, content) VALUES (NOW(), ?, ?)"
        query_update_unread_mails = "UPDATE users SET unread_mails = unread_mails + 1 WHERE username = ?;"
        try:
            self.cursor.execute(query_recv, (sender, message))
            self.cursor.execute(query_update_unread_mails, (receiver,))
            self.conn.commit()
        except Exception as e:
            print(f"Error while deactivating user: {e}")
            self.conn.rollback()

    def read_messages(self, users_name):
        users_name_table_name = f"{users_name}_mailbox"
        query_read_messages = f"""
        SELECT TO_CHAR(recive, 'FMMonth DD, YYYY at HH12:MI AM') AS received_formatted,
            is_new,
            sender,
            content
        FROM {users_name_table_name}
        ORDER BY recive DESC
        """
        query_update_mailbox = f"UPDATE {users_name_table_name} SET is_new = FALSE WHERE is_new  = TRUE;"
        query_update_unread_mails = "UPDATE users SET unread_mails = 0 where username = ?;"
        try:
            self.cursor.execute(query_read_messages)
            messages = self.cursor.fetchall()
            self.cursor.execute(query_update_mailbox)
            self.cursor.execute(query_update_unread_mails, (users_name,))
            self.conn.commit()
            return messages
        except Exception as e:
            print(f"Error while deactivating user: {e}")
            self.conn.rollback()
