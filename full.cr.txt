================================================================================

File: client.py

import socket
import orjson


class Client:
    def __init__(self, cl_host, cl_port, key, logic):
        self.host = cl_host
        self.port = cl_port
        self.verification_key = key
        self.logic = logic
        self.serv_conn = None

    def start_client(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.host, self.port))
            s.sendall(self.verification_key)
            self.serv_conn = orjson.loads(s.recv(1024))
            if self.serv_conn:
                print(orjson.loads(s.recv(1024)))
            while self.serv_conn:
                user_input = input("Type command: ")

                if user_input.lower() == "log":
                    if self.logic.logged_user:
                        print("You are allready logged in!")
                        continue

                if user_input.lower() == "stop":
                    send_data = {"command": "stop"}
                    s.sendall(orjson.dumps(send_data))
                    break

                send_data = self.logic.send_data_manager(user_input)
                s.sendall(orjson.dumps(send_data))
                recv_data = orjson.loads(s.recv(1024))
                self.logic.recv_data_manager(recv_data)


class Logic:
    def __init__(self):
        self.logged_user = None
        self.method_dict = {
            "create": self.new_user,
            "log": self.log_user,
            "log out": self.logout_user,
            "deactivate user": self.deactivate_user,
            "send": self.send_message
        }

    def send_data_manager(self, user_input):
        if user_input.lower() in self.method_dict:
            send_data = self.method_dict[user_input]()
        else:
            send_data = {"command": user_input}
        return send_data


    def recv_data_manager(self, recv_data):
        if "message" in recv_data:
            print(recv_data["message"])

        if "command" in recv_data:
            if recv_data["command"] == "log user":
                data = recv_data["data"]
                name = data
                self.logged_user = User(name, "*")
            elif recv_data["command"] == "users list":
                print("user/is admin/is active")
                for user, status in recv_data["data"].items():
                    print(user, status)
            elif recv_data["command"] == "mailbox content":
                for message in recv_data["data"]:
                    print(f"Send: {message[0]}")
                    if message[1] == True:
                        print("New")
                    else:
                        print("Readed")
                    print(f"From: {message[2]}")
                    print(f"Message: {message[3]}")

    def new_user(self):
        user_name = input("Enter user name: ")
        user_password = input("Enter user password: ")
        new_user = User(user_name, user_password)
        return {"command": "new user",
                "data": new_user.get_info()
        }

    def log_user(self):
        user_name = input("Enter user name: ")
        user_password = input("Enter user password: ")
        return {"command": "log user",
                "data": (user_name, user_password)
        }

    def logout_user(self):
        self.logged_user = None
        return {"command": "log out"}

    def deactivate_user(self):
        user_name = input("Enter user name: ")
        return {
            "command": "deactivate user",
            "data": user_name
        }

    def send_message(self):
        receiver = input("Enter receiver name: ")
        message = input("Enter message: ")
        return {
            "command": "send message",
            "data": (receiver, self.logged_user.name, message)
        }


class User:
    def __init__(self, name, password, admin=False, active=True):
        self.name = name
        self.password = password
        self.admin = admin
        self.active = active

    def get_info(self):
        return self.name, self.password, self.admin, self.active
                

host = "127.0.0.1"
port = 65432

if __name__ == "__main__":
    cl_logic = Logic()
    new_client = Client(host, port, b"1234", cl_logic)
    new_client.start_client()
================================================================================

File: config.py

from configparser import ConfigParser

def load_config(filename='database.ini', section='postgresql'):
    parser = ConfigParser()
    parser.read(filename)
    config = {}

    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            config[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))

    return config

if __name__ == '__main__':
    config = load_config()
    print(config)================================================================================

File: logic.py

import orjson
from sql_db import DatabaseConnection
from config import load_config
import bcrypt


class ServerMainLogic:
    def __init__(self):
        self.logged_user = None
        self.DBconn = DatabaseConnection("cs.db")
        self.DBconn.open_connection()
        self.method_dict = {
            "help": self.send_help,
            "new user": self.create_user,
            "log user": self.log_user,
            "log out": self.log_out,
        }

    def data_manager(self, data):
        recv_data = orjson.loads(data)
        if recv_data["command"] in self.method_dict:
            if "data" in recv_data:
                send_data = self.method_dict[recv_data["command"]](recv_data["data"])
            else:
                send_data = self.method_dict[recv_data["command"]]()
        elif self.logged_user and recv_data["command"] in self.logged_user.method_dict:
            if "data" in recv_data:
                send_data = self.logged_user.method_dict[recv_data["command"]](recv_data["data"])
            else:
                send_data = self.logged_user.method_dict[recv_data["command"]]()
        else:
            send_data = {"message": "Invalid command"}
        return orjson.dumps(send_data)

    def create_user(self, recv_data):
        create_attempt_name = recv_data[0]
        create_attempt_password = recv_data[1]
        hashed_password = self.hash_and_salt_password(create_attempt_password)
        result_creating_user = self.DBconn.commands.create_user(create_attempt_name, hashed_password)
        if not result_creating_user:
            send_data = {"message": "Your name is not unique"}
        else:
            send_data = {"message": "User was created successfully"}
        return send_data

    def log_user(self, recv_data):
        log_attempt_user_name = recv_data[0]
        log_attempt_user_password = recv_data[1]
        users_record = self.DBconn.commands.get_user_info(log_attempt_user_name)
        if users_record:
            valid_password = bcrypt.checkpw(log_attempt_user_password.encode("utf"), bytes(users_record[1]))
            if valid_password and users_record[3]:
                self.logged_user = LoggedUser(log_attempt_user_name, users_record[2], self.DBconn)
                send_data = {
                    "command": "log user",
                    "message": "Logged in successfully",
                    "data": log_attempt_user_name
                }
            elif not users_record[3]:
                send_data = {"message": "Your account was blocked by admin"}
            elif not valid_password:
                send_data = {"message": "Invalid password!"}
        else:
            send_data = {"message": "Wrong user name."}
        return send_data

    def log_out(self):
        send_data = {"message": "You have been logged out."}
        self.logged_user = None
        return send_data

    def greets(self):
        send_data = self.DBconn.commands.get_notification("greets")
        return orjson.dumps(send_data)

    def send_help(self):
        send_data = {"message": self.DBconn.commands.get_notification("help")}
        return send_data

    def hash_and_salt_password(self, password):
        byte_password = password.encode("utf-8")
        hashed_password = bcrypt.hashpw(byte_password, bcrypt.gensalt())
        return hashed_password


class LoggedUser:
    def __init__(self, name, is_admin, DBconnUser):
        self.DBconn = DBconnUser
        self.name = name
        self.is_admin = is_admin
        self.method_dict = {
            "users": self.get_all_users,
            "deactivate user": self.deactivate_user,
            "send message": self.recv_message,
            "messeges": self.read_messages
        }

    def get_all_users(self):
        if self.is_admin:
            all_users_info = self.DBconn.user_commands.get_all_users_info()
            users_info_for_admin = {}
            for record in all_users_info:
                users_info_for_admin[record[0]] = (record[1], record[2])
            send_data = {
                "command": "users list",
                "data": users_info_for_admin
            }
        else:
            send_data ={"message": "You are to 'short'. Become an admin first!"}
        return send_data

    def deactivate_user(self, recv_data):
        if self.is_admin:
            user = self.DBconn.commands.get_user_info(recv_data)
            if user:
                self.DBconn.user_commands.deactivate_user(recv_data)
                send_data = {"message": "User deactivated"}
            else:
                send_data = {"message": "Wrong users name"}
        else:
            send_data = {"message": "You are to 'short'. Become an admin first!"}
        return send_data

    def recv_message(self, recv_data):
        receivers_name = recv_data[0]
        senders_name = recv_data[1]
        message = recv_data[2]
        user_info = self.DBconn.commands.get_user_info(receivers_name)
        if user_info:
            if user_info[4] < 5 and len(message) <= 255:
                self.DBconn.user_commands.recv_message(receivers_name, senders_name, message)
                send_data = {"message": "Your message has been deliverd"}
            elif user_info[4] == 5:
                send_data = {"message": "Recivers mailbox is full"}
            elif len(message) > 255:
                send_data = {"message": "Your message is to long (max 255 characters)"}
        else:
            send_data = {"message": "Wrong recivers name"}
        return send_data

    def read_messages(self):
        logged_user_name = self.name
        messages = self.DBconn.user_commands.read_messages(logged_user_name)
        send_data = {"command": "mailbox content", "data": messages}
        return send_data
================================================================================

File: server.py

import socket
import logic
import orjson


class Server:
    def __init__(self, srv_host, srv_port, srv_logic):
        self.logic = srv_logic
        self.host = srv_host
        self.port = srv_port
        self.info = {"ver.": "1.1.0", "Development start date": "02.02.24"}
        self.client_conn = None

    def star_serv(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((self.host, self.port))
            s.listen()
            conn = s.accept()
            with conn[0]:
                self.check_ver_key(conn)
                conn[0].sendall(self.logic.greets())
                while self.client_conn:
                    recv_data = conn[0].recv(1024)
                    if orjson.loads(recv_data)["command"] == "stop":
                        self.logic.DBconn.close_connection()
                        break
                    if self.check_data_valid(orjson.loads(recv_data)):
                        send_data = self.logic.data_manager(recv_data)
                    else:
                        send_data = orjson.dumps({"message": "Invalid data"})
                    conn[0].sendall(send_data)

    def check_ver_key(self, conn):
        if not b"1234" == conn[0].recv(1024):
            conn[0].sendall(orjson.dumps(False))
            conn[0].close()
        else:
            self.client_conn = True
            conn[0].sendall(orjson.dumps(True))

    def check_data_valid(self, recv_data):
        """
        Recursively checks the validity of received data. The function supports nested
        data structures including dictionaries and lists. It evaluates each item within
        these structures using the `data_is_valid` method to determine if individual
        items meet specific validity criteria.

        Parameters:
        recv_data: The data to be validated, which can be a dictionary or a list.

        Returns:
        bool: True if all elements in the data structure are valid according to the
        `data_is_valid` method, False otherwise.
        """
        if isinstance(recv_data, dict):
            return all(self.check_data_valid(value) for value in recv_data.values())
        elif isinstance(recv_data, list):
            return all(self.check_data_valid(item) for item in recv_data)
        else:
            data_is_valid = self.data_is_valid(recv_data)
        return data_is_valid

    def data_is_valid(self, data):
        if isinstance(data, str):
            valid = data.isascii()
        elif isinstance(data, bool):
            valid = True
        else:
            valid = False
        return valid


host = "127.0.0.1"
port = 65432

logic = logic.ServerMainLogic()
new_server = Server(host, port, logic)
new_server.star_serv()
================================================================================

File: sql_db.py

import psycopg2
from config import load_config
import sqlite3


class DatabaseConnection:
    def __init__(self, db_path):
        self.db_path = db_path
        self.conn = None
        self.cur = None
        self.commands = None
        self.user_commands = None

    def open_connection(self):
        try:
            self.conn = sqlite3.connect(self.db_path)
            self.cur = self.conn.cursor()
            self.commands = DatabaseMainCommands(self.conn)
            self.user_commands = DatabaseUserCommands(self.conn)
            self.commands.create_tables_at_first_run()
            print("Connection to db opened successfully.")
        except Exception as e:
            print(f"An error occurred while connecting to the db: {e}")

    def close_connection(self):
        if self.cur is not None:
            self.cur.close()
            print("Cursor closed.")
        if self.conn is not None:
            self.conn.close()
            print("Connection to db closed.")


class DatabaseMainCommands:
    def __init__(self, conn):
        self.conn = conn
        self.cursor = self.conn.cursor()

    def create_tables_at_first_run(self):
        query = """
        CREATE TABLE IF NOT EXISTS users (
            username VARCHAR(255) NOT NULL PRIMARY KEY,
            password BYTEA NOT NULL,
            is_admin BOOLEAN NOT NULL DEFAULT FALSE,
            is_active BOOLEAN NOT NULL DEFAULT TRUE,
            unread_mails INTEGER NOT NULL DEFAULT 0 CHECK (unread_mails <= 5)
        );
        """
        self.cursor.execute(query)
        self.conn.commit()

    def get_notification(self, notification_key):
        query = "SELECT notification FROM notifications WHERE key = ?;"
        self.cursor.execute(query, (notification_key,))
        notification = self.cursor.fetchone()
        result = notification[0].replace("\\n", "\n")
        return result

    def create_user(self, username, password):
        query = "INSERT INTO users (username, password) VALUES (?, ?);"
        try:
            self.cursor.execute(query, (username, password))
            self.conn.commit()
            self.create_mailbox(username)
            return True
        except psycopg2.IntegrityError:
            self.conn.rollback()
            return False

    def create_mailbox(self, username):
        query = f"""
        CREATE TABLE IF NOT EXISTS {username}_mailbox (
            recive TIMESTAMP NOT NULL PRIMARY KEY,
            is_new BOOLEAN NOT NULL DEFAULT TRUE,
            sender VARCHAR(255) NOT NULL,
            content VARCHAR(255) NOT NULL
        );
        """
        self.cursor.execute(query)
        self.conn.commit()

    def get_user_info(self, user_name):
        query = """SELECT username, password, is_admin, is_active, unread_mails FROM users WHERE username = ?;"""
        self.cursor.execute(query, (user_name,))
        users_record = self.cursor.fetchone()
        return users_record


class DatabaseUserCommands:
    def __init__(self, conn):
        self.conn = conn
        self.cursor = self.conn.cursor()

    def get_all_users_info(self):
        query = "SELECT username, is_admin, is_active FROM users;"
        self.cursor.execute(query)
        all_users_info = self.cursor.fetchall()
        return all_users_info

    def deactivate_user(self, user_name):
        query = "UPDATE users SET is_active = FALSE WHERE username = ?;"
        try:
            self.cursor.execute(query, (user_name,))
            self.conn.commit()
        except Exception as e:
            print(f"Error while deactivating user: {e}")
            self.conn.rollback()

    def recv_message(self, receiver, sender, message):
        receiver_table_name = f"{receiver}_mailbox"
        query_recv = f"INSERT INTO {receiver_table_name} (recive, sender, content) VALUES (NOW(), ?, ?)"
        query_update_unread_mails = "UPDATE users SET unread_mails = unread_mails + 1 WHERE username = ?;"
        try:
            self.cursor.execute(query_recv, (sender, message))
            self.cursor.execute(query_update_unread_mails, (receiver,))
            self.conn.commit()
        except Exception as e:
            print(f"Error while deactivating user: {e}")
            self.conn.rollback()

    def read_messages(self, users_name):
        users_name_table_name = f"{users_name}_mailbox"
        query_read_messages = f"""
        SELECT TO_CHAR(recive, 'FMMonth DD, YYYY at HH12:MI AM') AS received_formatted,
            is_new,
            sender,
            content
        FROM {users_name_table_name}
        ORDER BY recive DESC
        """
        query_update_mailbox = f"UPDATE {users_name_table_name} SET is_new = FALSE WHERE is_new  = TRUE;"
        query_update_unread_mails = "UPDATE users SET unread_mails = 0 where username = ?;"
        try:
            self.cursor.execute(query_read_messages)
            messages = self.cursor.fetchall()
            self.cursor.execute(query_update_mailbox)
            self.cursor.execute(query_update_unread_mails, (users_name,))
            self.conn.commit()
            return messages
        except Exception as e:
            print(f"Error while deactivating user: {e}")
            self.conn.rollback()
================================================================================

File: __init__.py

