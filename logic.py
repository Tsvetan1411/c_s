import orjson
from sql_db import DatabaseConnection
from config import load_config
import bcrypt


class ServerMainLogic:
    def __init__(self):
        self.logged_user = None
        self.DBconn = DatabaseConnection("cs.db")
        self.DBconn.open_connection()
        self.method_dict = {
            "help": self.send_help,
            "new user": self.create_user,
            "log user": self.log_user,
            "log out": self.log_out,
        }

    def data_manager(self, data):
        recv_data = orjson.loads(data)
        if recv_data["command"] in self.method_dict:
            if "data" in recv_data:
                send_data = self.method_dict[recv_data["command"]](recv_data["data"])
            else:
                send_data = self.method_dict[recv_data["command"]]()
        elif self.logged_user and recv_data["command"] in self.logged_user.method_dict:
            if "data" in recv_data:
                send_data = self.logged_user.method_dict[recv_data["command"]](recv_data["data"])
            else:
                send_data = self.logged_user.method_dict[recv_data["command"]]()
        else:
            send_data = {"message": "Invalid command"}
        return orjson.dumps(send_data)

    def create_user(self, recv_data):
        create_attempt_name = recv_data[0]
        create_attempt_password = recv_data[1]
        hashed_password = self.hash_and_salt_password(create_attempt_password)
        result_creating_user = self.DBconn.commands.create_user(create_attempt_name, hashed_password)
        if not result_creating_user:
            send_data = {"message": "Your name is not unique"}
        else:
            send_data = {"message": "User was created successfully"}
        return send_data

    def log_user(self, recv_data):
        log_attempt_user_name = recv_data[0]
        log_attempt_user_password = recv_data[1]
        users_record = self.DBconn.commands.get_user_info(log_attempt_user_name)
        if users_record:
            valid_password = bcrypt.checkpw(log_attempt_user_password.encode("utf"), bytes(users_record[1]))
            if valid_password and users_record[3]:
                self.logged_user = LoggedUser(log_attempt_user_name, users_record[2], self.DBconn)
                send_data = {
                    "command": "log user",
                    "message": "Logged in successfully",
                    "data": log_attempt_user_name
                }
            elif not users_record[3]:
                send_data = {"message": "Your account was blocked by admin"}
            elif not valid_password:
                send_data = {"message": "Invalid password!"}
        else:
            send_data = {"message": "Wrong user name."}
        return send_data

    def log_out(self):
        send_data = {"message": "You have been logged out."}
        self.logged_user = None
        return send_data

    def greets(self):
        send_data = self.DBconn.commands.get_notification("greets")
        return orjson.dumps(send_data)

    def send_help(self):
        send_data = {"message": self.DBconn.commands.get_notification("help")}
        return send_data

    def hash_and_salt_password(self, password):
        byte_password = password.encode("utf-8")
        hashed_password = bcrypt.hashpw(byte_password, bcrypt.gensalt())
        return hashed_password


class LoggedUser:
    def __init__(self, name, is_admin, DBconnUser):
        self.DBconn = DBconnUser
        self.name = name
        self.is_admin = is_admin
        self.method_dict = {
            "users": self.get_all_users,
            "deactivate user": self.deactivate_user,
            "send message": self.recv_message,
            "messeges": self.read_messages
        }

    def get_all_users(self):
        if self.is_admin:
            all_users_info = self.DBconn.user_commands.get_all_users_info()
            users_info_for_admin = {}
            for record in all_users_info:
                users_info_for_admin[record[0]] = (record[1], record[2])
            send_data = {
                "command": "users list",
                "data": users_info_for_admin
            }
        else:
            send_data ={"message": "You are to 'short'. Become an admin first!"}
        return send_data

    def deactivate_user(self, recv_data):
        if self.is_admin:
            user = self.DBconn.commands.get_user_info(recv_data)
            if user:
                self.DBconn.user_commands.deactivate_user(recv_data)
                send_data = {"message": "User deactivated"}
            else:
                send_data = {"message": "Wrong users name"}
        else:
            send_data = {"message": "You are to 'short'. Become an admin first!"}
        return send_data

    def recv_message(self, recv_data):
        receivers_name = recv_data[0]
        senders_name = recv_data[1]
        message = recv_data[2]
        user_info = self.DBconn.commands.get_user_info(receivers_name)
        if user_info:
            if user_info[4] < 5 and len(message) <= 255:
                self.DBconn.user_commands.recv_message(receivers_name, senders_name, message)
                send_data = {"message": "Your message has been deliverd"}
            elif user_info[4] == 5:
                send_data = {"message": "Recivers mailbox is full"}
            elif len(message) > 255:
                send_data = {"message": "Your message is to long (max 255 characters)"}
        else:
            send_data = {"message": "Wrong recivers name"}
        return send_data

    def read_messages(self):
        logged_user_name = self.name
        messages = self.DBconn.user_commands.read_messages(logged_user_name)
        send_data = {"command": "mailbox content", "data": messages}
        return send_data
