import socket
import logic
import orjson


class Server:
    def __init__(self, srv_host, srv_port, srv_logic):
        self.logic = srv_logic
        self.host = srv_host
        self.port = srv_port
        self.info = {"ver.": "1.1.0", "Development start date": "02.02.24"}
        self.client_conn = None

    def star_serv(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((self.host, self.port))
            s.listen()
            conn = s.accept()
            with conn[0]:
                self.check_ver_key(conn)
                conn[0].sendall(self.logic.greets())
                while self.client_conn:
                    recv_data = conn[0].recv(1024)
                    if orjson.loads(recv_data)["command"] == "stop":
                        self.logic.DBconn.close_connection()
                        break
                    if self.check_data_valid(orjson.loads(recv_data)):
                        send_data = self.logic.data_manager(recv_data)
                    else:
                        send_data = orjson.dumps({"message": "Invalid data"})
                    conn[0].sendall(send_data)

    def check_ver_key(self, conn):
        if not b"1234" == conn[0].recv(1024):
            conn[0].sendall(orjson.dumps(False))
            conn[0].close()
        else:
            self.client_conn = True
            conn[0].sendall(orjson.dumps(True))

    def check_data_valid(self, recv_data):
        """
        Recursively checks the validity of received data. The function supports nested
        data structures including dictionaries and lists. It evaluates each item within
        these structures using the `data_is_valid` method to determine if individual
        items meet specific validity criteria.

        Parameters:
        recv_data: The data to be validated, which can be a dictionary or a list.

        Returns:
        bool: True if all elements in the data structure are valid according to the
        `data_is_valid` method, False otherwise.
        """
        if isinstance(recv_data, dict):
            return all(self.check_data_valid(value) for value in recv_data.values())
        elif isinstance(recv_data, list):
            return all(self.check_data_valid(item) for item in recv_data)
        else:
            data_is_valid = self.data_is_valid(recv_data)
        return data_is_valid

    def data_is_valid(self, data):
        if isinstance(data, str):
            valid = data.isascii()
        elif isinstance(data, bool):
            valid = True
        else:
            valid = False
        return valid


host = "127.0.0.1"
port = 65432

logic = logic.ServerMainLogic()
new_server = Server(host, port, logic)
new_server.star_serv()
